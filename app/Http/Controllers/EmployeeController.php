<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\EmployeeRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::orderBy('fname')->paginate(25);
        return response()->json($employee,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        Employee::create($request->all());
        return response()->json(['message'=>'Item add success'],201);
    }


    /**
     * search resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $fname = $request->fname?$request->fname:'%';
        $sname = $request->sname?$request->sname:'%';
        $pname = $request->pname?$request->pname:'%';
        $employee = Employee::where('fname','like','%'.$fname.'%')->where('sname','like','%'.$sname.'%')->where('pname','like','%'.$pname.'%')->orderBy('fname')->paginate(25);
        return response()->json($employee);
    }

    public function countWord(Request $request)
    {
        $this->validate($request, [
            'textForCountWord' => 'required|min:3|max:10000',
        ]);
        $textForCountWord = explode(" ",$request->textForCountWord);
        $words = array_count_values($textForCountWord);
        arsort($words);
        return response()->json(array_slice($words, 0, 1, true));
    }

    public function dateParse(Request $request)
    {
        $this->validate($request, [
            'dateForParse' => 'required|integer',
        ]);
        $date  = Carbon::createFromTimestamp($request->dateForParse)->format('i');
        return response()->json($date);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Employee::findOrFail($id)->update($request->all());
        return response()->json(['message'=>'Update success'],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::findOrFail($id)->delete();
        return response()->json(['message'=>'Delete success'],200);
    }
}
