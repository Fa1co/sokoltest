<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employeer';
    protected $fillable = [
        'fname',
        'sname',
        'pname'
    ];
}
